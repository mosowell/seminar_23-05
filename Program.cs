﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seminar2305
{

    interface ISwitchable 
    {
        void TurnOn();
        void TurnOff();
    }

    class FirstCl : ISwitchable
    {
        public void TurnOn()
        {
            Console.WriteLine("Включено!");
        }
        public void TurnOff()
        {
            Console.WriteLine("Выключено!");
        }
    }

    class SecondCl : ISwitchable
    {
        public void TurnOn()
        {
            Console.WriteLine("Как-то по-другому  Включено!");
        }
        public void TurnOff()
        {
            Console.WriteLine("Как-то по-другому Выключено!");
        }
    }

    class NamesGuard
    {
        public static void Print(List<string> names)
        {
            for (int i = 0; i < names.Count; i++)
            {
                if (i == 0)
                {
                    Console.Write(names[i]);
                }
                else
                {
                    Console.Write(", " + names[i]);
                }
            }
        }

        public static void Print(List<string> names, string separator)
        {
            for (int i = 0; i < names.Count; i++)
            {
                if (i == 0)
                {
                    Console.Write(names[i]);
                }
                else
                {
                    Console.Write(separator + names[i]);
                }
            }
        }
    }

    class ConvertMoney
    {

        public static int ToRub(int amount, string unit)
        {

            switch (unit)
            {
                case "USD":
                    return amount * 30; // Знаю, что слишком позитивно
                    break;

                default:
                    throw new Exception("Неизвестная валюта");
                    break;
            }
        }
        public static int ToUsd(int amount, string unit)
        {

            switch (unit)
            {
                case "Rub":
                    return amount / 30; 
                    break;

                default:
                    throw new Exception("Неизвестная валюта");
                    break;
            }
        }
    }
    public class Money
    {
        public int Amount { get; set; }
        public string Unit { get; set; }

        public Money(int amount, string unit)
        {
            Amount = amount;
            Unit = unit;
        }

        public static bool operator ==(Money a, Money b)
        {
            if (a.Amount == b.Amount && a.Unit == b.Unit)
            { 
            return true;
            }
            return false;
        }
        public static bool operator !=(Money a, Money b)
        {
            if (a.Amount == b.Amount && a.Unit == b.Unit)
            {
                return false;
            }
            return true;
        }

        public static Money operator +(Money a, Money b) //перегрузка оператора «+»
        {
            if (a.Unit == b.Unit)
            {
                return new Money(a.Amount + b.Amount, a.Unit);
            }
            else if (a.Unit == "USD" && b.Unit == "RUB")
            {
                return new Money(a.Amount + ConvertMoney.ToUsd(b.Amount, "RUB"), "RUB");
            }
            else
            {
                return new Money(a.Amount + ConvertMoney.ToRub(b.Amount, "USD"), "USD");
            }
        }

    }

    class MyCircle
    { 
        protected double x;
        protected double y;
        protected double radius;

        public MyCircle(double x, double y, double radius)
        {
            this.x = x;
            this.y = y;
            this.radius = radius;
        }

        public bool Equals(MyCircle obj) 
        {
            if (obj == null)
                return false;

            return obj.x == this.x && obj.y == this.y && obj.radius == this.radius;
        }

        public override int GetHashCode()
        {
            return (int)this.x + (int)this.y + (int)this.radius;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<string> names = new List<string>();

            names.Add("Jone");
            names.Add("Loker");
            names.Add("Pole");
            names.Add("Nale");
            names.Add("Ol");

            NamesGuard.Print(names);
            Console.WriteLine();
            NamesGuard.Print(names, "::");


            Money myMoney = new Money(100, "USD");
            Money yourMoney = new Money(100, "RUR");
            Money hisMoney = new Money(50, "USD");
            Money sum = myMoney + hisMoney; // 150 USD
            sum = yourMoney + hisMoney; // исключение - разные валюты
            Console.ReadLine();



        }
    }
}
